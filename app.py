

from flask import Flask, render_template, request
from flask_bootstrap import Bootstrap


app = Flask(__name__)
Bootstrap(app)


@app.route('/')
def index():
   return render_template('index.html')


@app.route('/itbms', methods=['GET','POST'])
def itbms():
   if request.method == 'GET':
      return render_template('itbms.html')
   elif request.method == 'POST':
      try:
         valor = float(request.form['valor'])
         resultado = valor * 0.07
      except:
         valor = -1.0
         resultado = valor
      return render_template('resultado.html', valor=valor, resultado=resultado)
   else:
      return 'Algo malo paso...'

if __name__ == '__main__':
   app.run()
